import React, {Component, Fragment} from 'react';
import {editContact, getContacts, removeContact} from "../../store/actions/actionsCreator";
import {connect} from "react-redux";
import './Contact.css'
import Modal from "../UI/Modal/Modal";

class Contact extends Component {
    state = {
        contacts: this.props.contacts,
        modal: false,
    };

    componentDidMount() {
        this.props.getContacts();
    }

    getModal = (index) => {
        let copy = {...this.state.contacts};
        return this.setState({modal: true, contacts: copy[index]})
    };

    closeModal = () => {
        return this.setState({modal: false,})
    };

    editContact = (event) => {
        event.preventDefault();

        const contact = {
            name: this.state.name,
            number: this.state.number,
            image: this.state.image,
            email: this.state.email,
        };

        this.props.editContact(this.props.id, contact);

        this.setState({...this.state, name: '', number: '', image: '', email: ''});

    };


    render() {
        const result = this.props.contacts;
        let contacts;
        let contact;

        if (result) {
            console.log(result);
            contacts = Object.keys(result).map(id => {
                return {...this.props.contacts[id]}
            });
            const stylesForInfo = {
                verticalAlign: 'top',
                lineHeight: '80px',
                backgroundColor: '#ccc',
                width: '40%'
            };
            contact = contacts.map((contact, key) => {
                return (
                    <Fragment key={key}>
                        <div className="contact-block" style={{display: 'flex', margin: '10px', padding: '4px 10px'}} key={key}>
                            <img src={contact.image} style={{display: 'inline-block', width: '80px', height: '80px'}} alt=""/>
                            <div className="info" onClick={() =>this.getModal(key)} style={stylesForInfo}>
                                <span style={{display: 'inline-block', margin: '0 10px',}} className="name">{contact.name}</span>
                            </div>
                        </div>
                        <Modal
                            name={contact.name}
                            email={contact.email}
                            number={contact.number}
                            image={contact.image}
                            closeModal={this.closeModal}
                            show={this.state.modal}
                            edit={this.editContact}
                            remove={this.props.removeContact}
                        />
                    </Fragment>

                )
            });
        }

        return (
            <div>
                {contact}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    contacts:  state.contacts
});

const mapDispatchToProps = dispatch => ({
    getContacts: () => dispatch(getContacts()),
    removeContact: (id) => dispatch(removeContact(id)),
    editContact: (id ,change) => dispatch(editContact(id, change))
});

export default connect(mapStateToProps, mapDispatchToProps)(Contact);