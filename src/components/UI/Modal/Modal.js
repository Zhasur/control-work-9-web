import React, {Fragment} from 'react';
import Backdrop from "../Backdrop/Backdrop";

import './Modal.css';

const Modal = props => {
  return (
    <Fragment>
      <Backdrop show={props.show} onClick={props.close} />
      <div
        className="Modal"
        key={props.key}
        style={{
          transform: props.show ? 'translateY(0)': 'translateY(-100vh)',
          opacity: props.show ? '1' : '0'
        }}
      >
          <div className="inner-modal">
              <span className="exit" onClick={props.closeModal}>x</span>
              <img src={props.image} style={{display: 'inline-block', width: '150px', height: '150px'}} alt="image"/>
              <div className="info" style={{display: 'inline-block'}}>
                  <span className="name">Name: {props.name}</span>
                  <span className="number">Phone: {props.number}</span>
                  <span className="email">Email: {props.email}</span>
              </div>
              <div className="btn-block">
                  <button id="edit" onClick={props.edit}>Edit</button>
                  <button id="delete" onClick={props.remove}>Delete</button>
              </div>
          </div>
      </div>
    </Fragment>
  );
};

export default Modal;
