import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createContact} from "../../store/actions/actionsCreator";

class AddForm extends Component {
    state = {
        name: '',
        number: '',
        image: '',
        email: '',
    };

    valueChanged = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    addContact = (event) => {

        event.preventDefault();
        const contact = {
            name: this.state.name,
            number: this.state.number,
            image: this.state.image,
            email: this.state.email,
        };

        this.props.createContact(contact);

        this.setState({...this.state, name: '', number: '', image: ''});

    };

    goToMainBlock = () => {
        this.props.history.push("/");
    };

    render() {
        return (
            <div style={{width: '500px', margin: '20px auto', padding: '100px 0', color: 'white'}}>
                <h2>Add new Contact</h2>
                <Form className="ContactForm" onSubmit={this.addContact}>
                    <FormGroup row>
                        <Label style={{color: '#000'}} for="name" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="name"
                                   id="name"
                                   value={this.state.name}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label style={{color: '#000'}} for="category" sm={2}>Phone</Label>
                        <Col sm={10}>
                            <Input type="number" name="number"
                                   value={this.state.number}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label style={{color: '#000'}} for="category" sm={2}>Email</Label>
                        <Col sm={10}>
                            <Input type="text" name="email"
                                   value={this.state.email}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label style={{color: '#000'}} for="category" sm={2}>Photo</Label>
                        <Col sm={10}>
                            <Input type="url" name="image"
                                   value={this.state.image}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button onClick={this.goToMainBlock} type="button">Back to contacts</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    image: state.contacts
});

const mapDispatchToProps = dispatch => ({
    createContact: (contact) => dispatch(createContact(contact))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddForm);