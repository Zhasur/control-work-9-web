import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import './App.css';

import Contacts from "./containers/Contacts/Contacts";
import ToolBar from "./components/Navigation/ToolBar/ToolBar";
import AddForm from "./components/AddForm/AddForm";

class App extends Component {
    render() {
        return (
            <Fragment>
                <ToolBar/>

                <div className="App" style={{paddingTop: '100px'}}>
                    <Switch>
                        <Route path="/" exact component={Contacts}/>
                        <Route path="/add-contact" component={AddForm}/>
                    </Switch>
                </div>
            </Fragment>

        );
    }
}

export default App;
