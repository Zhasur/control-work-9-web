export const INIT_REQUEST = 'INIT_REQUEST';
export const INIT_SUCCESS = 'INIT_SUCCESS';
export const INIT_ERROR = 'INIT_ERROR';

export const initRequest = () => ({type: INIT_REQUEST});
export const initSuccess = (contacts) => ({type: INIT_SUCCESS, contacts});
export const initError = (error) => ({type: INIT_ERROR, error});


