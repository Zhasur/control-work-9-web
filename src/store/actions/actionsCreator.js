import axios from '../../axios-contacts';
import {initSuccess, initRequest, initError} from "./actionsType";

export const getContacts = () => {
    return dispatch => {
        dispatch(initRequest());
        axios.get('contacts.json').then(response => {
            dispatch(initSuccess(response.data));
        },error => {
            dispatch(initError(error))
        })
    }
};

export const createContact= (contact) => {
    return dispatch => {
        dispatch(initRequest());
        axios.post('contacts.json', contact).then(
            error => dispatch(initError(error))
        );
    }
};

export const editContact = (id, change) => {
    return dispatch => {
        dispatch(initRequest());
        axios.put('contacts/' + id + '.json', change).then(() => {
            dispatch(getContacts())
        })
    }
};

export const removeContact = (id) => {
    return dispatch => {
        dispatch(initRequest());
        axios.delete('contacts/' + id + '.json').then(() => {
            dispatch(getContacts())
        })
    }
};

