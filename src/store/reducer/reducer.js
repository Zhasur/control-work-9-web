import {INIT_SUCCESS} from "../actions/actionsType";

const initialState = {
    contacts: null,
};

const ContactsReducer = (state = initialState, action) => {
    switch (action.type) {

        case INIT_SUCCESS:

            return {...state, contacts: action.contacts};

        default:
            return state
    }
};


export default ContactsReducer